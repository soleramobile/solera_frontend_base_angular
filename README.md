# Solera Frontend Angular

Este proyecto representa una estructura base de los proyectos de Solera basados en el Framework [Angular](https://angular.io/)


### Primeraso pasos
Debes abrir la terminar y 

- Ejecutar: ```npm install```
- Ejecutar: ```cp .env.example .env``` o crear el archivo `.env` basado en `env.example`
- Ejecutar: ```npm run start```
- Abrir el navegador en la ruta ```localhost:4200```

### Documentación
Para ver la documentación completa del proyecto se hará uso de [StoryBook](https://storybook.js.org/).

- Ejecutar: ```npm run storybook```
