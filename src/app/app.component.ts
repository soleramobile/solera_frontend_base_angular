import { Component } from '@angular/core';
import { fadeAnimation } from '@shared/animations/fade.animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  animations: [fadeAnimation],
})
export class AppComponent {
  title = 'Solera Frontend Base Angular';

  i = 0;
}
