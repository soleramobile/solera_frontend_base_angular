import { Injectable } from '@angular/core';
import { ApiService } from '@core/api/api.service';
import { Observable } from 'rxjs';
import { PostI } from './posts.model';

const MAX_ITEMS = 10;

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  constructor(private apiService: ApiService) {}

  /**
   * Obtiener todos los post
   */
  getAll(page: number = 1): Observable<PostI[]> {
    const start = MAX_ITEMS * (page - 1) + 1;
    const limit = start + MAX_ITEMS - 1;
    const query = {
      start,
      limit,
    };
    return <Observable<PostI[]>> this.apiService.get('/posts', query);
  }
}
