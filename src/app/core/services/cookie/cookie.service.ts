import {
  Injectable, Optional, Inject, PLATFORM_ID,
} from '@angular/core';
import { CookieService as NgCookieService, CookieOptionsProvider } from 'ngx-cookie';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class CookieService extends NgCookieService {
  constructor(
    @Optional() @Inject(REQUEST) private request: any,
    @Inject(PLATFORM_ID) private platformId: any,
    _optionsProvider: CookieOptionsProvider,
  ) {
    super(_optionsProvider);
  }

  /**
   *
   * Esta función permite consultar la cookie desde el servidor (SSR) o desde el cliente
   *
   * @param name Nombre de la cookie a consultar
   */
  getCookie(name: string): (any) {
    // Consulta de la cookie del browser (cliente)
    if (
      isPlatformBrowser(this.platformId)
      && !!this.get(name)
    ) {
      return this.getObject(name);
    } if (!isPlatformBrowser(this.platformId) && !!this.request) {
      // Consulta de la cookie desde el servidor
      // Parse del string de todas las cookies a un objeto
      // Luego busca en el propiedad "name"

      if (this.request.headers.cookie) {
        const str = this.request.headers.cookie.split('; ');
        const result: any = {};
        for (let i = 0; i < str.length; i += 1) {
          const cur:any[] = str[i].split('=');
          // eslint-disable-next-line prefer-destructuring
          result[cur[0]] = cur[1];
        }
        if (result.data) return JSON.parse(unescape(result[name]));
      }
    }

    return null;
  }
}
