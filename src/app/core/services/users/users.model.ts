export interface AddressI {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: {
    lat: number;
    lng: number;
  };
}

export interface CompanyI {
  name: string;
  catchPhrase: string;
  bs: string;
}

export interface UserI {
  id: number;
  name: string;
  username: string;
  email: string;
  address: AddressI;
  phone: string;
  website: string;
  company: CompanyI;
}
