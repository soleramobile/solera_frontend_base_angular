import { Injectable } from '@angular/core';
import { ApiService } from '@core/api/api.service';
import { Observable } from 'rxjs';
import { UserI } from './users.model';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private apiService: ApiService) {}

  /**
   * Obtiener todos los usuarios
   */
  getAll(): Observable<UserI[]> {
    return <Observable<UserI[]>> this.apiService.get('/users');
  }
}
