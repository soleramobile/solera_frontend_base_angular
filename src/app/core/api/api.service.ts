import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '@environments/environment';
import { catchError } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { HttpResponseI } from './api.model';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient, private router: ActivatedRoute) {}

  get(url, params = {}, options = {}): Observable<HttpResponseI | any> {
    return this.http
      .get<HttpResponseI | any>(environment.API_URL + url, {
      ...{ params },
      ...this.httpOptions,
      ...options,
    })
      .pipe(catchError(this.handleError));
  }

  post(url, body = {}, options = this.httpOptions): Observable<HttpResponseI> {
    return this.http
      .post<HttpResponseI | any>(environment.API_URL + url, body, {
      ...options,
    })
      .pipe(catchError(this.handleError));
  }

  put(url, body = {}, options = {}): Observable<any> {
    return this.http
      .put(environment.API_URL + url, body, {
        ...this.httpOptions,
        ...options,
      })
      .pipe(catchError(this.handleError));
  }

  delete(url, options = {}): Observable<any> {
    return this.http
      .delete(environment.API_URL + url, {
        ...this.httpOptions,
        ...options,
      })
      .pipe(catchError(this.handleError));
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(errorMessage);
  }
}
