export interface HttpErrorI {
  errorCode?: number,
  errorMessage?: string,
  errorMessageDetail: any
}

export interface HttpResponseI {
  success: boolean;
  body?: {
    successMessage?: string;
    data?: any;
  },
  error?: HttpErrorI
}

export interface HttpErrorResponseI {
  success: boolean;
  error?: HttpErrorI
}
