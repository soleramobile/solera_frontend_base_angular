import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from '@core/services/cookie/cookie.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private cookieService:CookieService) {}

  intercept(req: HttpRequest<any> = null, next: HttpHandler = null) {
    // Get the auth token from the service.
    const authToken: string = this.cookieService.getCookie('token');
    if (!authToken) return next.handle(req);

    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({
      headers: req.headers.set('Authorization', `Bearer ${authToken}`),
    });

    // send cloned request with header to the next handler.
    return next.handle(authReq);
  }
}
