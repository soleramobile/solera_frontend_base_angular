import { Injectable } from '@angular/core';
import {
  Router,
  Route,
} from '@angular/router';
import { CookieService } from '@core/services/cookie/cookie.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard {
  constructor(public router: Router, private cookieService: CookieService) {}

  canLoad(route: Route): boolean {
    const authToken = this.cookieService.getCookie('token');

    if (route.path === '/auth/login') {
      if (authToken) {
        this.router.navigate(['/']);
        return false;
      }
    } else if (route.path !== '/auth/login' && !authToken) {
      this.router.navigate(['/auth/login']);
      return false;
    }

    return true;
  }
}
