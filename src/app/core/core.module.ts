import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CookieService } from './services/cookie/cookie.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [CookieService],
})
export class CoreModule {}
