import { Injectable, Inject } from '@angular/core';
import { AlertI } from '@shared/modules/alert/alert.model';
import {
  BehaviorSubject, Subscription, Observable, of,
} from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ElementsService {
  elements$: BehaviorSubject<{ element: AlertI.IAlertElement; active: boolean }[]>;

  subscription: Subscription[] = [];

  config: AlertI.IConfigAlert;

  constructor(@Inject('CONFIG_ALERT') config: AlertI.IConfigAlert) {
    this.config = config;
    this.elements$ = new BehaviorSubject<
    { element: AlertI.IAlertElement; active: boolean }[]
    >([]);
  }

  /*
   * Observable con un arreglo de las alertas mostradas
   */
  get elements(): Observable<{ element: AlertI.IAlertElement; active: boolean }[]> {
    return this.elements$.asObservable();
  }

  /**
   * Agregar un alerta
   * @param element
   */
  addElement(element: AlertI.IAlertElement) {
    const { value } = this.elements$;
    value.push({ active: true, element });
    this.elements$.next(value);
    this.addInterval();
  }

  /**
   * Eliminar un alerta
   * @param index index de alerta a eliminar
   */
  removeElement(index) {
    const { value } = this.elements$;
    value[index].active = false;
    this.elements$.next(value);
    this.subscription[index].unsubscribe();
  }

  /**
   * Agregar contador de elemento para que desaparezca
   */
  addInterval() {
    this.subscription.push(
      of(this.elements$.value.length)
        .pipe(delay(this.config.time))
        .subscribe((length) => {
          this.removeElement(Number(length) - 1);
        }),
    );
  }

  /**
   * Detener el contador de tiempos para desaparecer los alertas
   */
  deleteAllInterval(index) {
    this.subscription[index].unsubscribe();
  }
}
