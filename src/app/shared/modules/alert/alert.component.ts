import {
  Component,
  OnInit,
  ElementRef,
  Inject,
} from '@angular/core';
import {
  trigger,
  transition,
  query,
  style,
  stagger,
  animate,
  keyframes,
} from '@angular/animations';
import { AlertI } from './alert.model';
import { ElementsService } from './elements.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  animations: [
    // Trigger animation cards array
    trigger('cardAnimation', [
      // Transition from any state to any state
      transition('* => *', [
        // Initially the all cards are not visible
        query(':enter', style({ opacity: 0 }), { optional: true }),

        // Each card will appear sequentially with the delay of 300ms
        query(
          ':enter',
          stagger('300ms', [
            animate(
              '.5s ease-in',
              keyframes([
                style({ opacity: 0, transform: 'translateY(-50%)', offset: 0 }),
                style({
                  opacity: 0.5,
                  transform: 'translateY(-10px) scale(1.1)',
                  offset: 0.3,
                }),
                style({ opacity: 1, transform: 'translateY(0)', offset: 1 }),
              ]),
            ),
          ]),
          { optional: true },
        ),

        // Cards will disappear sequentially with the delay of 300ms
        query(
          ':leave',
          stagger('300ms', [
            animate(
              '500ms ease-out',
              keyframes([
                style({ opacity: 1, transform: 'scale(1.1)', offset: 0 }),
                style({ opacity: 0.5, transform: 'scale(.5)', offset: 0.3 }),
                style({ opacity: 0, transform: 'scale(0)', offset: 1 }),
              ]),
            ),
          ]),
          { optional: true },
        ),
      ]),
    ]),
  ],
})
export class AlertComponent implements OnInit {
  elements$: {
    element: AlertI.IAlertElement;
    active: boolean;
  }[] = [];

  constructor(
    private el: ElementRef,
    private elementService: ElementsService,
    @Inject('CONFIG_ALERT') config: AlertI.IConfigAlert,
  ) {
    switch (config.position) {
      case 'top-left':
        this.el.nativeElement.style.top = '0px';
        this.el.nativeElement.style.left = '0px';
        break;
      case 'top-right':
        this.el.nativeElement.style.top = '0px';
        this.el.nativeElement.style.right = '0px';
        break;
      case 'bottom-left':
        this.el.nativeElement.style.bottom = '0px';
        this.el.nativeElement.style.left = '0px';
        break;
      case 'bottom-right':
        this.el.nativeElement.style.bottom = '0px';
        this.el.nativeElement.style.right = '0px';
        break;
      default:
        this.el.nativeElement.style.right = '0px';
        this.el.nativeElement.style.bottom = '0px';
        break;
    }

    this.elementService.elements.subscribe((data) => {
      this.elements$ = data.filter((e) => e.active);
    });
  }

  ngOnInit() {}

  addElement(element:AlertI.IAlertElement) {
    this.elementService.addElement(element);
  }

  /**
   * Eliminar una alerta
   * @param i
   */
  remove(i) {
    this.elementService.removeElement(i);
  }

  /**
   * Reiniciar los contadores que eliminan las alertas
   */
  mouseout() {
    this.elementService.addInterval();
  }

  /**
   * Eliminar todos los intervalos de tiempo para eliminar el listado de alertas
   * Es decir, Poner una pausa a la desaparición de alertas
   */
  mouseover(index) {
    this.elementService.deleteAllInterval(index);
  }
}
