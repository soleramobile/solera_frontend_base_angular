import {
  NgModule,
  ModuleWithProviders,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './alert.component';
import { AlertI } from './alert.model';


export const CONFIG_INITIAL: AlertI.IConfigAlert = {
  position: 'bottom-right',
  time: 4000,
};

@NgModule({
  declarations: [AlertComponent],
  imports: [CommonModule],
  entryComponents: [AlertComponent],
  exports: [AlertComponent],
})
export class AlertModule {
  static forRoot(config: AlertI.IConfigAlert = CONFIG_INITIAL): ModuleWithProviders {
    return {
      ngModule: AlertModule,
      providers: [
        {
          provide: 'CONFIG_ALERT',
          useValue: { ...CONFIG_INITIAL, ...config },
        },
      ],
    };
  }
}
