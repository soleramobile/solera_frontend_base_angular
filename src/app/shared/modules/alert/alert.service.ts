import {
  Injectable,
  Inject,
  ComponentRef,
  ComponentFactoryResolver,
  ApplicationRef,
  Injector,
  EmbeddedViewRef,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { AlertI } from './alert.model';
import { AlertComponent } from './alert.component';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  componentRef: ComponentRef<AlertComponent>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
    @Inject(PLATFORM_ID) private platformId: any,
  ) {}

  add(alert: AlertI.IAlertElement | any = {}): void {
    if (isPlatformBrowser(this.platformId)) {
      if (!this.componentRef) {
        /**
         * Add component alert after body html
         */
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
          AlertComponent,
        );
        const componentRef = componentFactory.create(this.injector);
        this.appRef.attachView(componentRef.hostView);

        const domElem = (componentRef.hostView as EmbeddedViewRef<any>)
          .rootNodes[0] as HTMLElement;

        document.body.appendChild(domElem);

        this.componentRef = componentRef;
      }

      this.componentRef.instance.addElement(alert);
    }
  }
}
