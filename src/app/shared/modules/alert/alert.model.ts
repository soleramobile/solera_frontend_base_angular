// this ghost module has 1 name in top level
export declare module AlertI {
  /**
   * Interface one Element Alert
   */
  interface IAlertElement {
    /** Alert title */
    title: string;
    /** Alert description */
    description?: string;
    /** Status color, it define background color */
    status?: 'default' | 'success' | 'info' | 'danger' | 'warning';
  }

  /**
   * Alert config
   */
  interface IConfigAlert {
    /** Container position of alerts elements */
    position?: 'top-right' | 'top-left' | 'bottom-left' | 'bottom-right';
    /** One Alert element's time life */
    time?: number;
  }
}
