import {
  NgModule,
  ComponentFactoryResolver,
  ApplicationRef,
  Injector,
  EmbeddedViewRef,
  ModuleWithProviders,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { IConfigModal } from './modal';

@NgModule({
  declarations: [ModalComponent],
  imports: [CommonModule],
  entryComponents: [ModalComponent],
})
export class ModalModule {
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
  ) {
    /**
     * Add component alert after body html
     */
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      ModalComponent,
    );
    const componentRef = componentFactory.create(this.injector);
    this.appRef.attachView(componentRef.hostView);

    const domElem = (componentRef.hostView as EmbeddedViewRef<any>)
      .rootNodes[0] as HTMLElement;
    document.body.appendChild(domElem);
  }

  static forRoot(config: IConfigModal = {}): ModuleWithProviders {
    return {
      ngModule: ModalModule,
      providers: [
        {
          provide: 'CONFIG_MODAL',
          useValue: { ...{}, ...config },
        },
      ],
    };
  }
}
