import {
  Component,
  OnInit,
  ViewChild,
  Input,
  ElementRef,
  ComponentFactoryResolver,
} from '@angular/core';
import { ModalService } from './modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() component: ElementRef;

  @ViewChild('modalcontent', { static: true }) refContent;

  constructor(
    public ms: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
  ) {}

  ngOnInit() {
    this.ms.currentComponent.subscribe((component: any) => {
      if (component) {
        // Create component dynamically inside the ng-template
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
          component,
        );
        this.refContent.createComponent(componentFactory);
      }
    });
  }
}
