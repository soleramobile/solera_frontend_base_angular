import {
  Injectable, Inject, ElementRef,
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IConfigModal } from './modal';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  isShow$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  component$: BehaviorSubject<ElementRef> = new BehaviorSubject(null);

  config$: BehaviorSubject<IConfigModal>;

  constructor(@Inject('CONFIG_MODAL') config: IConfigModal) {
    this.config$ = new BehaviorSubject<IConfigModal>(config);
  }

  get isShow(): Observable<boolean> {
    return this.isShow$.asObservable();
  }

  get currentComponent(): Observable<ElementRef> {
    return this.component$.asObservable();
  }

  show(component:ElementRef): void {
    this.component$.next(component);
    this.isShow$.next(true);
  }

  close(): void {
    this.isShow$.next(false);
    this.component$.next(null);
  }
}
