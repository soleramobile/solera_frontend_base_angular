import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { LoadingDirective } from './directives/loading/loading.directive';
import { StepsComponent } from './components/steps/steps.component';
import { ScrollingDirective } from './directives/scrolling/scrolling.directive';

@NgModule({
  declarations: [
    NotfoundComponent,
    MaintenanceComponent,
    LoadingDirective,
    StepsComponent,
    ScrollingDirective,
  ],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [
    NotfoundComponent,
    MaintenanceComponent,
    ReactiveFormsModule,
    LoadingDirective,
    StepsComponent,
    ScrollingDirective,
  ],
})
export class SharedModule {}
