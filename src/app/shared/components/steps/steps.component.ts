import {
  Component, OnInit, Inject, PLATFORM_ID,
} from '@angular/core';
import { Observable } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { StepsService } from './steps.service';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss'],
})
export class StepsComponent implements OnInit {
  current: Observable<number> = this.stepsService.current$;

  steps: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private stepsService: StepsService,
    @Inject(PLATFORM_ID) private platformId: any,
  ) {
    this.stepsService.listenRouter();
  }

  ngOnInit() {
    this.steps = [...this.route.routeConfig.children].splice(1);
    this.stepsService.current.subscribe((current:number) => {
      const n = this.steps.length;
      if (isPlatformBrowser(this.platformId)) {
        const bar: any = document.getElementsByClassName('bar__progress')[0];
        bar.style.width = `${(100 / (n - 1)) * (current - 1)}%`;
      }
    });
  }
}
