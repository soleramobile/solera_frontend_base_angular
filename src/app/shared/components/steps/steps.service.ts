import { Injectable, Optional } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class StepsService {
  current$: BehaviorSubject<number>;

  data$:BehaviorSubject<any>;

  constructor(
    @Optional() private router: Router,
  ) {
    this.current$ = new BehaviorSubject(1);
    this.data$ = new BehaviorSubject(null);
  }

  listenRouter() {
    this.router.events
      .pipe(filter((event:any) => event instanceof ActivationEnd))
      .subscribe((event:any) => {
        if (event.snapshot.data.step) {
          this.setData(event.snapshot.data);
          this.setCurrent(event.snapshot.data.step);
        }
      });
  }

  get current(): Observable<number> {
    return this.current$.asObservable();
  }

  get data(): Observable<any> {
    return this.data$.asObservable();
  }


  setCurrent(current) {
    this.current$.next(current);
  }

  setData(data) {
    this.data$.next(data);
  }
}
