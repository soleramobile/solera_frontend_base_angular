import {
  Directive,
  Input,
  HostListener,
  ElementRef,
  OnInit,
} from '@angular/core';

@Directive({
  selector: '[scroll-directive]',
})
export class ScrollingDirective implements OnInit {
  elements: any[] = [];

  current: number = 0;

  lastPx: number = 0;

  constructor(private el: ElementRef) {}

  ngOnInit() {
    this.elements = <any>document.querySelectorAll('.content');
  }

  @Input() height: number;

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    // Va arriba

    const middle = this.elements[this.current].offsetTop
      + this.elements[this.current].offsetHeight / 8;

    if (
      window.pageYOffset > this.lastPx
      && window.pageYOffset > middle
      && window.pageYOffset > this.elements[this.current].offsetTop
    ) {
      // Hacia abajo
      window.scrollTo({
        top: this.elements[this.current + 1].offsetTop,
        behavior: 'smooth',
      });
      this.current += 1;
    } else if (
      window.pageYOffset < this.lastPx
      && window.pageYOffset < middle
      && window.pageYOffset < this.elements[this.current].offsetTop
    ) {
      // Va Arriba
      window.scrollTo({
        top: this.elements[this.current - 1].offsetTop,
        behavior: 'smooth',
      });
      this.current -= 1;
    }

    this.lastPx = window.pageYOffset;
  }
}
