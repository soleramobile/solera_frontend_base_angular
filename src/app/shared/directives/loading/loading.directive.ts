import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';


@Directive({
  selector: '[appLoading]',
})
export class LoadingDirective implements OnChanges {
  /** Se mostrará el Spinner */
  @Input() show: boolean = false;

  /** Color del spinner */
  @Input() color: string = '#ffffff';

  constructor(
    private el: ElementRef,
    @Inject(PLATFORM_ID) private platformId: any,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.show && isPlatformBrowser(this.platformId)) {
      if (changes.show.currentValue === 'false') {
        const e = document.getElementsByClassName('container-loader')[0];
        if (e) e.parentNode.removeChild(e);
      } else if (changes.show.currentValue === 'true') {
        this.el.nativeElement.style.position = 'relative';
        this.el.nativeElement.insertAdjacentHTML(
          'beforeend',
          '<div class="container-loader"><div class="loader"></div></div>',
        );
        const e:any = document.getElementsByClassName('loader')[0];
        e.style['border-left'] = `1.1em solid ${this.color}`;
      }
    }
  }
}
