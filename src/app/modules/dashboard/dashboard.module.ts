import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CookieService } from '@core/services/cookie/cookie.service';
import { SharedModule } from '@shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UsersComponent } from './pages/users/users.component';
import { PostsComponent } from './pages/posts/posts.component';
@NgModule({
  declarations: [SidebarComponent, UsersComponent, PostsComponent],
  imports: [CommonModule, DashboardRoutingModule, SharedModule],
  providers: [CookieService],
})
export class DashboardModule {}
