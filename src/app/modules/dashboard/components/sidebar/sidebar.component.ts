import { Component } from '@angular/core';
import { fadeAnimation } from '@shared/animations/fade.animation';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [fadeAnimation],
})
export class SidebarComponent {
  routes: any = [
    { path: '/dashboard', title: 'Posts', exact: true },
    { path: '/dashboard/users', title: 'Usuarios', exact: false },
    { path: '/dashboard/steps', title: 'Steps', exact: false },
  ];
}
