import { Component, OnInit } from '@angular/core';
import { UsersService } from '@core/services/users/users.service';
import { UserI } from '@core/services/users/users.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users: UserI[] | any = [{}, {}, {}, {}];

  loading:boolean = true;

  constructor(private postService: UsersService) {
    this.postService.getAll().subscribe((data) => {
      this.users = <UserI[]>data;
      this.loading = false;
    });
  }

  ngOnInit() {}
}
