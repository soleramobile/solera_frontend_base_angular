import { Component } from '@angular/core';
import { StepsService } from '@shared/components/steps/steps.service';

@Component({
  selector: 'app-example-steps',
  templateUrl: './example-steps.component.html',
  styleUrls: ['./example-steps.component.scss'],
})
export class ExampleStepsComponent {
  constructor(private stepsService: StepsService) {
    this.stepsService.listenRouter();
  }
}
