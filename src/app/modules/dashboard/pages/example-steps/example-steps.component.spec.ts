import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleStepsComponent } from './example-steps.component';

describe('ExampleStepsComponent', () => {
  let component: ExampleStepsComponent;
  let fixture: ComponentFixture<ExampleStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExampleStepsComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
