import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExampleStepsComponent } from './example-steps.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';

const routes: Routes = [
  {
    path: '',
    component: ExampleStepsComponent,
    children: [
      { path: '', redirectTo: '/dashboard/steps/1', pathMatch: 'full' },
      { path: '1', component: Step1Component, data: { step: 1 } },
      { path: '2', component: Step2Component, data: { step: 2 } },
      { path: '3', component: Step3Component, data: { step: 3 } },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExampleStepsRoutingModule {}
