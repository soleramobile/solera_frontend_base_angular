import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { ExampleStepsRoutingModule } from './example-steps-routing.module';
import { ExampleStepsComponent } from './example-steps.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { Step1Component } from './step1/step1.component';


@NgModule({
  declarations: [ExampleStepsComponent, Step1Component, Step2Component, Step3Component],
  imports: [
    CommonModule,
    ExampleStepsRoutingModule,
    SharedModule,
  ],
})
export class ExampleStepsModule { }
