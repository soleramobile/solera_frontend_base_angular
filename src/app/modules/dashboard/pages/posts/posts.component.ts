import { Component, OnInit } from '@angular/core';
import { PostsService } from '@core/services/posts/posts.service';
import { PostI } from '@core/services/posts/posts.model';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
})
export class PostsComponent implements OnInit {
  posts: PostI[] | any = [{}, {}, {}, {}];

  loading: boolean = true;

  constructor(private postService: PostsService) {}

  ngOnInit() {
    this.postService.getAll().subscribe((data) => {
      this.loading = false;
      this.posts = <PostI[]>data;
    });
  }
}
