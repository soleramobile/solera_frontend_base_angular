import { Component } from '@angular/core';
import { fadeAnimationRotate } from '@shared/animations/fade.animation';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [fadeAnimationRotate],
})
export class LayoutComponent {
  public getRouterOutletState(outlet:RouterOutlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
