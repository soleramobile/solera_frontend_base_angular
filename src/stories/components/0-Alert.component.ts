import { Component, OnInit, Input } from '@angular/core';
import { AlertService } from '@shared/modules/alert/alert.service';

@Component({
  selector: 'app-alert',
  template: `
    <div class="p-5 pl-0">
      <button (click)="addAlert('info')" class="mr-3 btn btn-info">
        Click me
      </button>
      <button (click)="addAlert('danger')" class="mr-3 btn btn-danger">
        Click me
      </button>
      <button (click)="addAlert('success')" class="mr-3 btn btn-success">
        Click me
      </button>
      <button (click)="addAlert('warning')" class="mr-3 btn btn-warning">
        Click me
      </button>
    </div>
  `,
})
export class AppAlert implements OnInit {
  @Input() data: any = null;

  constructor(private alertService: AlertService) {}

  ngOnInit() {}

  addAlert(status) {
    this.alertService.add({
      title: `Título del Alert - ${status}`,
      description: `Descripción del Alert - ${status}`,
      status,
    });
  }
}
