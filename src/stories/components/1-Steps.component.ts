import { Component } from '@angular/core';
import { StepsService } from '@shared/components/steps/steps.service';

@Component({
  selector: 'app-step-storybookx',
  template: `
    <div class="p-0">
      <button (click)="addStep(-1)" class="mr-3 btn btn-info" *ngIf="current > 1">
        Prev
      </button>
      <button (click)="addStep(1)" class="mr-3 btn btn-info" *ngIf="current < 3">
        Next
      </button>
    </div>
  `,
})
export class AppSteps {
  current: number = 1;

  constructor(private stepsService: StepsService) {
  }

  addStep(step) {
    if (this.current > 0 && this.current < 4) {
      this.stepsService.setCurrent(this.current + step);
      this.current += step;
    }
  }
}
