import { Component } from '@angular/core';

@Component({
  selector: 'app-loading',
  template: `
    <div class="ml-2 mt-2">
      <button
        (click)="changeLoading()"
        class="mr-3 btn btn-info w-25"
        appLoading
        [disabled]="isLoading"
        show="{{ isLoading }}"
      >
        {{ isLoading ? "&nbsp;" : "Click me" }}
      </button>
      <button *ngIf="isLoading" (click)="changeLoading()"> reset</button>
    </div>
  `,
})
export class AppLoading {
  isLoading: boolean = false;

  changeLoading() {
    this.isLoading = !this.isLoading;
  }
}
