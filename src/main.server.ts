import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';

if (environment.ENVIRONMENT === 'production') {
  enableProdMode();
}

// eslint-disable-next-line import/named
export { AppServerModule } from './app/app.server.module';
export { ngExpressEngine } from '@nguniversal/express-engine';
export { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
