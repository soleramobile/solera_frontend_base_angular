module.exports = {
  stories: ["../src/stories/**/*.stories.(js|mdx|ts)"],
  addons: [
    "@storybook/addon-actions",
    "@storybook/addon-links",
    "@storybook/addon-notes",
    "@storybook/addon-docs"
  ],
};
