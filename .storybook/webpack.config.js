const path = require('path');

module.exports = {
	resolve: {
		modules: [
			"node_modules",
			path.join(__dirname, "../"),
			path.join(__dirname, "../src"),
		],
		alias: {
			'@environments': path.resolve(__dirname, '../src/environments'),
            '@modules': path.resolve(__dirname, '../src/app/modules/'),
      	    '@core': path.resolve(__dirname, '../src/app/core'),
      		'@shared': path.resolve(__dirname, '../src/app/shared/'),
			'styles': path.resolve(__dirname, '../src/styles')
		}
	}
};