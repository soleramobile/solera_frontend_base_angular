# Changelog

Todos los cambios notables del proyecto deben estar en este archivo. 

## [0.5.1] - 2020-05-20
### Fixed
- Integrando en el server.ts las variables de entorno

## [0.5.1] - 2020-03-10
- Correcciones de compilación del storybook

## [0.5.0] - 2020-03-05
- Integración de Eslint para verificación de sintaxis del código

### Added
- Creación de la directive loading


## [0.5.0] - 2020-03-06

### Added
- Integración de Eslint
- Corrección de módulo steps


## [0.4.0] - 2020-02-10

### Added
- Creación de la directive loading

## [0.3.0] - 2020-02-10

### Added
- Integrar storybook
- Integración de los componentes Alert y Steps 

## [0.2.0] - 2020-02-04

### Added
- Comunicación con backend para los modulos de posts y users
- Integrando las funcionalidades del proyecto base.
- Creando modulos de ejemplo: Home, Login y Dashboard.

## [0.1.0] - 2020-01-30

### Added
- Integrando Storybook
- Creación dínamica de los environments basados del archivo `.env`

## [0.0.0] - 2020-01-30

### Added

- Creación inicial del proyecto