/* eslint-disable no-restricted-syntax */
/* eslint-disable no-plusplus */
/* eslint-disable guard-for-in */
/* eslint-disable no-console */
import * as fs from 'fs';
import * as dotenv from 'dotenv';
import * as commonData from './src/environments/environment.main';

dotenv.config();

/**
 * Función lee el archivo .env y si no existe lo crea apartie de .env.example
 */
const readEnvironments = async () => {
  if (!fs.existsSync('.env')) await fs.copyFileSync('.env.example', '.env');

  const envConfig = await dotenv.parse(fs.readFileSync('.env'));
  const response = {};

  for (const k in envConfig) {
    // Excluyendo el puerto
    if (k !== 'PORT') {
      // Si se encuentra en un servidor distinto de desarrollo se apunta /api
      // Haciendo uso del Proxy.
      if (k === 'API_URL' && process.env.ENVIRONMENT !== 'develop') {
        response[k] = '/api';
      } else {
        response[k] = envConfig[k];
      }
    }
  }

  return response;
};

/**
 * Escribe el archivo environments
 * @param file archivo destino
 * @param text texto a escribir en el archivo
 */
const writEnvironments = async (file, text) => {
  const writeFile = fs.writeFileSync;
  await writeFile(file, text, { encoding: 'utf8' });
};

(async () => readEnvironments().then(async (environment) => {
  const textToWrite = JSON.stringify({ ...environment, ...commonData.default }, null, 2);

  const envConfigFile = `export const environment = ${textToWrite};`;

  // Si se ejecuta producción y no existe environments.ts
  // se genera primero el archivo environments.ts
  if (process.env.ENVIRONMENT === 'production') {
    await writEnvironments('./src/environments/environment.ts', envConfigFile);
  }
  await writEnvironments(`./src/environments/environment${process.env.ENVIRONMENT === 'production' ? '.prod' : ''}.ts`, envConfigFile);
}))();
