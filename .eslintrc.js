module.exports = {
  extends: ['airbnb-typescript/base'],
  settings: {
    "import/resolver": {
      "eslint-import-resolver-custom-alias": {
        "alias": {
          "@modules": "app/modules",
          "@environments": "src/environments",
          "@core": "app/core",
          "@shared": "app/shared",
        },
        "extensions": [".js", ".ts"],
      },
      // alias: {
      //   map: [
      //     ['@modules','app/modules']
      //     ['@environments', 'src/environments']
      //     // ['@core','app/core'],
      //     // ['@shared','app/shared'],
      //   ],
      //   extensions: ['.ts', '.js']
      // },
    }
  },
  parserOptions: {
    project: './tsconfig.json',
  },
  ignorePatterns: [
    'node_modules/*',
    'e2e/*',
    'karma.conf.js',
    'dist/',
    'src/polyfills.ts',
    'src/test.ts',
    'src/app/app.component.spec.ts',
    'src/app/stories/'
  ],
  rules: {
    "import/prefer-default-export": "off",
    "import/named": "off",
    "no-console": "off",
    "no-alert": "error",
    "class-methods-use-this": "off",
    "import/no-unresolved": "off",
  },
};